# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [v0.4.0] - 2021-09-10

### Added

- Add ``parse_known_args`` to CommandParser to mimic argparse.ArgumentParser functionality.

## [v0.3.0] - 2021-09-10

### Added

- Allow adding commands under arbitrary tuple keys, e.g., for ``app pod get --namespace hello``
- Allow adding `BaseCommand` to `CommandParser`

## [v0.2.0] - 2021-09-02

### Added

- The ``CommandParser`` class
- The ``BaseCommand`` and ``Command`` classes
