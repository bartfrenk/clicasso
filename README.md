# clicasso

- [Package description](docs/source/description.rst)
- [Changelog](CHANGELOG.md)

To create the Sphinx documentation run:

    pip install -r requirements/docs.txt
    make docs
