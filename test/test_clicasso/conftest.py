import pytest
from clicasso.parser import CommandParser


@pytest.fixture
def parser() -> CommandParser:
    return CommandParser()
