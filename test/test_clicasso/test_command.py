import pytest

from clicasso import BaseCommand, ParseError


class TestBaseCommand:
    def test_command_slug_is_kebab_cased_name_by_default(self, parser):
        class FakeCommand(BaseCommand):
            pass

        parser.add(FakeCommand)
        cmd = parser.parse(["fake-command"])
        assert isinstance(cmd, FakeCommand)

    def test_add_parse_slug_into_command(self, parser):
        class Fake(BaseCommand):
            @classmethod
            def slug(cls):
                return "slug"

        parser.add(Fake)
        cmd = parser.parse(["slug"])
        assert isinstance(cmd, Fake)

    def test_parse_boolean_flag(self, parser):
        class Fake(BaseCommand):
            foo: bool

        parser.add(Fake)

        cmd = parser.parse(["fake"])
        assert cmd.foo is False

        cmd = parser.parse(["fake", "--foo"])
        assert cmd.foo is True

    def test_parse_fails_on_missing_required_field_without_default(self, parser):
        class Fake(BaseCommand):
            foo: str

        parser.add(Fake)
        with pytest.raises(ParseError):
            parser.parse(["fake"])

    def test_parse_does_not_fail_on_missing_field_with_default(self, parser):
        class Fake(BaseCommand):
            foo: str = "foo"

        parser.add(Fake)
        try:
            parser.parse(["fake"])
        except ParseError as exc:
            assert False, exc

    def test_parse_integer(self, parser):
        class Fake(BaseCommand):
            foo: int

        parser.add(Fake)
        cmd = parser.parse(["fake", "--foo", "1"])
        assert cmd.foo == 1
