from pydantic import BaseModel
from clicasso import CommandParser


class TestCommandParser:
    def test_connect_base_model_parser(self):
        class Foo(BaseModel):
            bar: str

        parser = CommandParser.from_commands(Foo)
        cmd = parser.parse(["foo", "--bar", "baz"])
        assert isinstance(cmd, Foo)
        assert cmd.bar == "baz"

    def test_connect_under_non_singleton_slug(self):
        class Foo(BaseModel):
            bar: str

        parser = CommandParser()
        parser.add(Foo, ("foo", "quux"))
        cmd = parser.parse(["foo", "quux", "--bar", "baz"])
        assert isinstance(cmd, Foo)
        assert cmd.bar == "baz"

    def test_connect_multiple_commands_under_same_root_key(self):
        class Foo(BaseModel):
            bar: str

        class Corge(BaseModel):
            grault: str

        parser = CommandParser()
        parser.add(Foo, ("quux", "foo"))
        parser.add(Corge, ("quux", "corge"))

        cmd = parser.parse(["quux", "foo", "--bar", "baz"])
        assert isinstance(cmd, Foo)
        assert cmd.bar == "baz"

        cmd = parser.parse(["quux", "corge", "--grault", "baz"])
        assert isinstance(cmd, Corge)
        assert cmd.grault == "baz"

    def test_parse_known_args_returns_non_parsed_args(self):
        class Foo(BaseModel):
            bar: str

        parser = CommandParser()
        parser.add(Foo, ("quux", "foo"))
        cmd, rest = parser.parse_known_args(["quux", "foo", "--bar", "baz", "x", "--y", "z"])
        assert isinstance(cmd, Foo)
        assert cmd.bar == "baz"
        assert rest == ["x", "--y", "z"]
