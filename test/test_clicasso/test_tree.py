from clicasso.tree import Node


class TestNode:
    def test_put_of_singleton_on_empty_node(self):
        root = Node("")
        root.put(("foo",), lambda s, t: (s,) + t)
        assert root.children["foo"] == Node(("", "foo"))
        assert len(root.children) == 1

    def test_put_of_singleton_on_non_empty_node(self):
        node = Node("foo")
        root = Node("", children={"foo": node})
        root.put(("foo",), lambda s, t: (s,) + t)
        assert id(root.children["foo"]) == id(node)
        assert len(root.children) == 1
